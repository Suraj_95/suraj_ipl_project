/**
 * @param {Array} matches // Records of matches.
 * @param {Array} deliveries // Records of deliveries.
 * @returns {Object} Return extra runs conceded per team in the year 2016.
 */
function extraRunByEachTeam(matches, deliveries) {
	/**
	 * Pushing match_id for season 2016 into an array.
	 */
	const match_id = matches.reduce((match, elem) => {
		const season = elem.season;
		if (season == 2016) {
			match.push(elem.id);
		}
		return match;
	}, []);

	/**
	 * Logic for each teams conceding extra runs.
	 */
	return deliveries.reduce((deliverie, elem) => {
		const deliverie_id = elem.match_id;
		const bowlingTeam = elem.bowling_team;
		const extraRuns = elem.extra_runs;
		if (match_id.includes(deliverie_id)) {
			if (deliverie[bowlingTeam] !== undefined) {
				deliverie[bowlingTeam] += parseInt(extraRuns);
			} else {
				deliverie[bowlingTeam] = parseInt(extraRuns);
			}
		}
		return deliverie;
	}, {});
}

module.exports = extraRunByEachTeam;
