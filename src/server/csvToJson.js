/**
 * Node package to read or right files.
 */
const fs = require("fs");

/**
 * csvtojson module is a comprehensive nodejs csv parser to convert csv to json or column arrays.
 */
const csv = require("csvtojson");

/**
 * Path for csv files.
 */
const MATCHES_FILE_PATH = "./src/data/matches.csv";
const DELIVERIES_FILE_PATH = "./src/data/deliveries.csv";

/**
 * Path for json files.
 */
const JSON_MATCHES_FILE_PATH = "./src/data/matches.json";
const JSON_DELIVERIES_FILE_PATH = "./src/data/deliveries.json";

/**
 * Logic to convert csv to json.
 */
csv()
	.fromFile(MATCHES_FILE_PATH)
	.then((matches) => {
		fs.writeFile(
			JSON_MATCHES_FILE_PATH,
			JSON.stringify(matches),
			"utf8",
			(err) => {
				if (err) {
					console.error(err);
				}
			}
		);
		csv()
			.fromFile(DELIVERIES_FILE_PATH)
			.then((deliveries) => {
				fs.writeFile(
					JSON_DELIVERIES_FILE_PATH,
					JSON.stringify(deliveries),
					"utf8",
					(err) => {
						if (err) {
							console.error(err);
						}
					}
				);
			});
	});
