/**
 * Node package to read or right files.
 */
const fs = require("fs");

/**
 * Path of json(data) file.
 */
const matchesJSON = "./src/data/matches.json";
const deliveriesJSON = "./src/data/deliveries.json";

/**
 * Calling the function.
 */
const matchesPlayedPerYear = require("./matchesPlayedPerYear");
const matchesWonbyTeamsPerYear = require("./matchesWonbyTeamsPerYear");
const extraRunByEachTeam = require("./extraRunByEachTeam");
const topEconomicalBowlers = require("./topEconomicalBowlers");
const teamsWonBothTossAndMatch = require("./teamsWonBothTossAndMatch");
const highestNumberOfPlayerOfTheMatch = require("./highestNumberOfPlayerOfTheMatch");
const strikeRate = require("./strikeRate");
const highestNumberOfTimePlayerDismissed = require("./highestNumberOfTimePlayerDismissed");
const bowlersBestEconomyInSuperOvers = require("./bowlersBestEconomyInSuperOvers");

/**
 * Path to dump json data.
 */
const JSON_MatchesPerYear_FILE_PATH =
	"./src/public/output/matchesPlayedPerYear.json";
const JSON_MatchesWonbyTeamsPerYear_FILE_PATH =
	"./src/public/output/matchesWonbyTeamsPerYear.json";
const JSON_ExtraRunByEachTeam_FILE_PATH =
	"./src/public/output/extraRunByEachTeam.json";
const JSON_TopEconomicalBowlers_FILE_PATH =
	"./src/public/output/topEconomicalBowlers.json";
const JSON_TeamsWonBothTossAndMatch_FILE_PATH =
	"./src/public/output/teamsWonBothTossAndMatch.json";
const JSON_HighestNumberOfPlayerOfTheMatch_FILE_PATH =
	"./src/public/output/highestNumberOfPlayerOfTheMatch.json";
const JSON_StrikeRate_FILE_PATH = "./src/public/output/strikeRate.json";
const JSON_PlayerDismissed_FILE_PATH =
	"./src/public/output/highestNumberOfTimePlayerDismissed.json";
const JSON_BestEconomyInSuperOvers_FILE_PATH =
	"./src/public/output/bowlersBestEconomyInSuperOvers.json";

/**
 * Parsing the json data into array.
 */
const matches = JSON.parse(fs.readFileSync(matchesJSON));
const deliveries = JSON.parse(fs.readFileSync(deliveriesJSON));

/**
 * Storing the data of each function called.
 */
let matchesPlayedPerYearObj = matchesPlayedPerYear(matches);
let matchesWonbyTeamsPerYearObj = matchesWonbyTeamsPerYear(matches);
let extraRunByEachTeamObj = extraRunByEachTeam(matches, deliveries);
let topEconomicalBowlersObj = topEconomicalBowlers(matches, deliveries);
let teamsWonBothTossAndMatchObj = teamsWonBothTossAndMatch(matches);
let highestNumberOfPlayerOfTheMatchObj =
	highestNumberOfPlayerOfTheMatch(matches);
let strikeRateObj = strikeRate(deliveries);
let highestNumberOfTimePlayerDismissedObj =
	highestNumberOfTimePlayerDismissed(deliveries);
let bowlersBestEconomyInSuperOversObj =
	bowlersBestEconomyInSuperOvers(deliveries);

/**
 * Writing the data into given json file's.
 */
fs.writeFile(
	JSON_MatchesPerYear_FILE_PATH,
	JSON.stringify(matchesPlayedPerYearObj),
	"utf8",
	(err) => {
		if (err) {
			console.error(err);
		}
	}
);

/**
 * Writing the data into given json file's.
 */
fs.writeFile(
	JSON_MatchesWonbyTeamsPerYear_FILE_PATH,
	JSON.stringify(matchesWonbyTeamsPerYearObj),
	"utf8",
	(err) => {
		if (err) {
			console.error(err);
		}
	}
);

/**
 * Writing the data into given json file's.
 */
fs.writeFile(
	JSON_ExtraRunByEachTeam_FILE_PATH,
	JSON.stringify(extraRunByEachTeamObj),
	"utf8",
	(err) => {
		if (err) {
			console.error(err);
		}
	}
);

/**
 * Writing the data into given json file's.
 */
fs.writeFile(
	JSON_TopEconomicalBowlers_FILE_PATH,
	JSON.stringify(topEconomicalBowlersObj),
	"utf8",
	(err) => {
		if (err) {
			console.error(err);
		}
	}
);

/**
 * Writing the data into given json file's.
 */
fs.writeFile(
	JSON_TeamsWonBothTossAndMatch_FILE_PATH,
	JSON.stringify(teamsWonBothTossAndMatchObj),
	"utf8",
	(err) => {
		if (err) {
			console.error(err);
		}
	}
);

/**
 * Writing the data into given json file's.
 */
fs.writeFile(
	JSON_HighestNumberOfPlayerOfTheMatch_FILE_PATH,
	JSON.stringify(highestNumberOfPlayerOfTheMatchObj),
	"utf8",
	(err) => {
		if (err) {
			console.error(err);
		}
	}
);

/**
 * Writing the data into given json file's.
 */
fs.writeFile(
	JSON_StrikeRate_FILE_PATH,
	JSON.stringify(strikeRateObj),
	"utf8",
	(err) => {
		if (err) {
			console.error(err);
		}
	}
);

/**
 * Writing the data into given json file's.
 */
fs.writeFile(
	JSON_PlayerDismissed_FILE_PATH,
	JSON.stringify(highestNumberOfTimePlayerDismissedObj),
	"utf8",
	(err) => {
		if (err) {
			console.error(err);
		}
	}
);

/**
 * Writing the data into given json file's.
 */
fs.writeFile(
	JSON_BestEconomyInSuperOvers_FILE_PATH,
	JSON.stringify(bowlersBestEconomyInSuperOversObj),
	"utf8",
	(err) => {
		if (err) {
			console.error(err);
		}
	}
);
