/**
 *
 * @param {Array} deliveries Records of deliveries.
 * @returns {Object} Returns an object containing most number of time a Player Dismissed by another players.
 */
function highestNumberOfTimePlayerDismissed(deliveries) {
	/**
	 * Storing into object player Dismissed name with its count.
	 */
	const result = deliveries.reduce((deliverie, elem) => {
		let playerDismissed = elem.player_dismissed;
		let filder = elem.filder;
		if (filder != "" && playerDismissed != "") {
			if (deliverie[playerDismissed] !== undefined) {
				deliverie[playerDismissed] += 1;
			} else {
				deliverie[playerDismissed] = 1;
			}
		}
		return deliverie;
	}, {});

	/**
	 * Converting into array and Sorting and finding player with most number of time dismissd.
	 */
	let resArr = Object.entries(result);
	resArr.sort((a, b) => b[1] - a[1]);
	resArr = resArr.slice(0, 1);

	/**
	 * Storing into object with player name and dismissed count.
	 */
	return resArr.reduce((name, element) => {
		name[element[0]] = element[1];
		return name;
	}, {});
}

module.exports = highestNumberOfTimePlayerDismissed;
