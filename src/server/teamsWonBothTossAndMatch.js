/**
 *
 * @param {Arrays} matches Records of matches.
 * @returns {Object} Returns an Object containing Team's who Won both toss and match.
 */
function teamsWonBothTossAndMatch(matches) {
	return matches.reduce((match, ele) => {
		const winner = ele.winner;
		const tossWinner = ele.toss_winner;
		if (tossWinner === winner) {
			if (match[tossWinner] !== undefined) {
				match[tossWinner] += 1;
			} else {
				match[tossWinner] = 1;
			}
		}
		return match;
	}, {});
}
module.exports = teamsWonBothTossAndMatch;
