/**
 *
 * @param {Array} matches Record of matched played.
 * @returns {Object} Return an Object having team's name with total number of wins in each season.
 */
function matchesWonbyTeamsPerYear(matches) {
	/**
	 * Iterating over matches and storing team names with its winning count into Object.
	 */
	const result = matches.reduce((acc, match) => {
		const season = match.season;
		const winner = match.winner;
		if (acc[season] === undefined) {
			acc[season] = {};
		} else {
			if (acc[season][winner] !== undefined) {
				acc[season][winner] += 1;
			} else {
				acc[season][winner] = 1;
			}
		}
		return acc;
	}, {});

	/**
	 * Logic to creating an empty array for each teams.
	 */
	const overAllTeam = {};
	let teams = [];
	for (let year in result) {
		for (let key in result[year]) {
			if (overAllTeam[key] === undefined) {
				if (key !== "") {
					teams.push(key);
					overAllTeam[key] = [];
				}
			}
		}
	}

	/**
	 * Pushing the matches won count into array by each team throughout the season and returning the result.
	 */
	for (let key in result) {
		for (let name of teams) {
			if (result[key].hasOwnProperty(name)) {
				overAllTeam[name].push(result[key][name]);
			} else {
				overAllTeam[name].push(0);
			}
		}
	}
	return overAllTeam;
}

module.exports = matchesWonbyTeamsPerYear;
