/**
 *
 * @param {Array} matches Record of matches played.
 * @returns {Object} Returns Player name with most number of times Player of the match in each season.
 */
function highestNumberOfPlayerOfTheMatch(matches) {
	/**
	 * Iterating the array to create Object having Player name's with man of the match count in each season.
	 */
	const result = matches.reduce((acc, match) => {
		const season = match.season;
		const manOfTheMatch = match.player_of_match;
		if (acc[season] === undefined) {
			acc[season] = {};
		} else {
			if (acc[season][manOfTheMatch] !== undefined) {
				acc[season][manOfTheMatch] += 1;
			} else {
				acc[season][manOfTheMatch] = 1;
			}
		}
		return acc;
	}, {});

	/**
	 * Converting into array.
	 */
	let keyValue = Object.entries(result);

	/**
	 * Sorting and finding the top player with most man of the match in each season.
	 */
	const resObj = keyValue.reduce((acc, elements) => {
		let arr = Object.entries(result[elements[0]]);
		arr.sort((a, b) => b[1] - a[1]);
		arr = arr.slice(0, 1);
		acc[elements[0]] = arr[0][0];
		return acc;
	}, {});
	return resObj;
}

module.exports = highestNumberOfPlayerOfTheMatch;
