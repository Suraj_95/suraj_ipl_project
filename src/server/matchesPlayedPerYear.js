/**
 * Function to calculate matches played per year.
 * @param {Array} matches
 * @returns {Object} Returned an Object with Matches played in given year.
 *
 */
function matchesPlayedPerYear(matches) {
	return matches.reduce((match, elem) => {
		const season = elem.season;
		if (match[season] !== undefined) {
			match[season] += 1;
		} else {
			match[season] = 1;
		}
		return match;
	}, {});
}

module.exports = matchesPlayedPerYear;
