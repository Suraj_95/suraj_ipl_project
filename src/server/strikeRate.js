/**
 *
 * @param {Array} deliveries Records of deliveries.
 * @returns {Object} Player names in each season with best Strike rate.
 */
function strikeRate(deliveries) {
	const runScoredByBatsmen = {};
	const ballPlayedByBatsmen = {};

	/**
	 * Iterating through deliveries.
	 */
	deliveries.forEach((deliverie) => {
		let match_id = deliverie.match_id;
		let batsman = deliverie.batsman;
		let batsman_runs = deliverie.batsman_runs;
		let batsman_faced = deliverie.ball;
		if (parseInt(match_id) >= 1 && parseInt(match_id) <= 59) {
			let season = 2017;
			runScored(season);
			ballsPlayed(season);
		}
		if (parseInt(match_id) >= 60 && parseInt(match_id) <= 117) {
			let season = 2008;
			runScored(season);
			ballsPlayed(season);
		}
		if (parseInt(match_id) >= 118 && parseInt(match_id) <= 174) {
			let season = 2009;
			runScored(season);
			ballsPlayed(season);
		}
		if (parseInt(match_id) >= 175 && parseInt(match_id) <= 234) {
			let season = 2010;
			runScored(season);
			ballsPlayed(season);
		}
		if (parseInt(match_id) >= 235 && parseInt(match_id) <= 307) {
			let season = 2011;
			runScored(season);
			ballsPlayed(season);
		}
		if (parseInt(match_id) >= 308 && parseInt(match_id) <= 381) {
			let season = 2012;
			runScored(season);
			ballsPlayed(season);
		}
		if (parseInt(match_id) >= 382 && parseInt(match_id) <= 457) {
			let season = 2013;
			runScored(season);
			ballsPlayed(season);
		}
		if (parseInt(match_id) >= 458 && parseInt(match_id) <= 517) {
			let season = 2014;
			runScored(season);
			ballsPlayed(season);
		}
		if (parseInt(match_id) >= 518 && parseInt(match_id) <= 576) {
			let season = 2015;
			runScored(season);
			ballsPlayed(season);
		}
		if (parseInt(match_id) >= 577 && parseInt(match_id) <= 636) {
			let season = 2016;
			runScored(season);
			ballsPlayed(season);
		}

		/**
		 *
		 * @param {variable} season  Each year.
		 *
		 * Creating object containing Player name's with runs scored in each season.
		 */
		function runScored(season) {
			if (runScoredByBatsmen[season] === undefined) {
				runScoredByBatsmen[season] = {};
			}
			if (runScoredByBatsmen[season][batsman] !== undefined) {
				runScoredByBatsmen[season][batsman] += parseInt(batsman_runs);
			} else {
				runScoredByBatsmen[season][batsman] = parseInt(batsman_runs);
			}
		}

		/**
		 *
		 * @param {variable} season
		 *
		 * Creating object containing Player name's with ball played in each season.
		 */
		function ballsPlayed(season) {
			if (ballPlayedByBatsmen[season] === undefined) {
				ballPlayedByBatsmen[season] = {};
			}
			if (ballPlayedByBatsmen[season][batsman] !== undefined) {
				if (batsman_faced !== undefined)
					ballPlayedByBatsmen[season][batsman] += 1;
			} else {
				ballPlayedByBatsmen[season][batsman] = 1;
			}
		}
	});

	/**
	 * Function calculating strick rate of each batsmen in each season and storing into an Object.
	 */
	let strikeRateObj = {};
	getStrikeRate(runScoredByBatsmen, ballPlayedByBatsmen);
	function getStrikeRate(runScoredByBatsmen, ballPlayedByBatsmen) {
		for (let runScoredYear in runScoredByBatsmen) {
			for (let runScoredYearName in runScoredByBatsmen[runScoredYear]) {
				let run = runScoredByBatsmen[runScoredYear][runScoredYearName];
				let ball = ballPlayedByBatsmen[runScoredYear][runScoredYearName];
				let strickRateBatsman = ((run / ball) * 100).toFixed(2);
				if (strickRateBatsman != 0) {
					strikeRateObj[runScoredYear] = {
						[runScoredYearName]: strickRateBatsman,
					};
				}
			}
		}
	}
	return strikeRateObj;
}
module.exports = strikeRate;
