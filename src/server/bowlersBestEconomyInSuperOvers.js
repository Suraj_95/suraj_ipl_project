/**
 *
 * @param {Array} deliveries Records of deliveries.
 * @returns {Object} Return a bowler with best economy.
 */
function bowlersBestEconomyInSuperOvers(deliveries) {
	/**
	 * Iterating through deliveries and Storing the bowler name's and total runs given by the bowler's into Object.
	 */
	let totalRun = deliveries.reduce((acc, deliverie) => {
		let bowler = deliverie.bowler;
		let runs = deliverie.total_runs;
		if (bowler != "" && runs != 0) {
			if (acc[bowler] !== undefined) {
				acc[bowler] += parseInt(runs);
			} else {
				acc[bowler] = parseInt(runs);
			}
		}
		return acc;
	}, {});

	/**
	 * Iterating through deliveries and Storing the bowler name's and total ball bowled by the bowler's into Object.
	 */
	let overBalled = deliveries.reduce((acc, deliverie) => {
		let bowler = deliverie.bowler;
		let superOver = deliverie.is_super_over;
		if (bowler != "" && superOver != "") {
			if (acc[bowler] !== undefined) {
				if (superOver) acc[bowler] += 1;
			} else {
				acc[bowler] = 1;
			}
		}
		return acc;
	}, {});

	/**
	 * Converting into array.
	 */
	let totalRunArr = Object.entries(totalRun);
	let overBalledArr = Object.entries(overBalled);

	/**
	 * Logic to calculate number of overs bowled by each bowler.
	 */
	overBalledArr.reduce((acc, over) => {
		over[1] = parseFloat((over[1] / 6).toFixed(2));
		return acc;
	}, []);

	/**
	 * Logic to calculate calculate economy for each bowler.
	 */
	totalRunArr.reduce((acc, runs, index) => {
		runs[1] = parseFloat((runs[1] / overBalledArr[index][1]).toFixed(2));
		return acc;
	}, []);

	/**
	 * Sorting and fetching a bowler with best economy.
	 */
	totalRunArr.sort(function (a, b) {
		return a[1] - b[1];
	});
	totalRunArr = totalRunArr.slice(0, 1);

	/**
	 * Storing the player name with its economy in Object and returning it.
	 */
	const newResult = totalRunArr.reduce((acc, elem) => {
		acc[elem[0]] = elem[1];
		return acc;
	}, {});
	return newResult;
}
module.exports = bowlersBestEconomyInSuperOvers;
